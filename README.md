### Komentarze
- Jakosc warstwy UI zostala zignorowana
- Data jest sformatowana jako LocalDate, ale wyswietla sie w formacie ktory przychodzi z serwera (nie bylo sprecyzowane jaki ma byc)
- Myslalem aby uzyc flow dla odczytu z bazy danych, ale nie wykonujemy na liscie zadnych operacji, traktujemy jedynie jako cache, tak wiec uznalem flow za over-kill w tym przypadku

### Do usprawnienia
- Dodac wiecej testow (nie chcialem poswiecac wiecej niz 4h na zadanie)
- Lepsza obsluga webview i zwiazanych z wyswietlaniem rzeczy (np. aktualnie celowo pozwolilem wyswietlac linki http, mimo iz np. w realnym use case raczej bylyby to linki https)
- Konfiguracja R8 i zaciemnianie kodu
- Bardziej zaawansowana obsluga bledow z API i bazy danych
- Dla pelniejszego clean architecture mozna by dodac jeszcze mapper obiektu w bazie danych na obiekt uzywany w ui
- Wiecej interface

### Link do apk
https://www.dropbox.com/s/y9gsctsqjvnzc1w/app-debug.apk