package com.example.futuremind.di

import com.example.futuremind.BuildConfig
import com.example.futuremind.data.remote.typeadapter.LocalDateTypeAdapter
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.TypeAdapter
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.time.LocalDate
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
abstract class NetworkModule {

    companion object {

        @Singleton
        @Provides
        fun provideLocalDateTypeAdapter(): TypeAdapter<LocalDate> {
            return LocalDateTypeAdapter()
        }

        @Singleton
        @Provides
        fun provideGson(localDateTypeAdapter: TypeAdapter<LocalDate>): Gson {
            return GsonBuilder()
                .registerTypeAdapter(LocalDate::class.java, localDateTypeAdapter.nullSafe())
                .create()
        }

        @Singleton
        @Provides
        fun provideRetrofit(okHttpClient: OkHttpClient, gson: Gson): Retrofit {
            return Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(BuildConfig.API_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
        }

        @Singleton
        @Provides
        fun provideHttpClient(): OkHttpClient {
            return OkHttpClient.Builder()
                .readTimeout(BuildConfig.API_TIMEOUT, TimeUnit.SECONDS)
                .build()
        }
    }
}