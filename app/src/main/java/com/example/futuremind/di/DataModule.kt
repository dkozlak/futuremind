package com.example.futuremind.di

import android.content.Context
import androidx.room.Room
import com.example.futuremind.BuildConfig
import com.example.futuremind.data.ItemRepository
import com.example.futuremind.data.ItemRepositoryImpl
import com.example.futuremind.data.StringFormatter
import com.example.futuremind.data.local.LocalItemSource
import com.example.futuremind.data.local.db.ItemsDatabase
import com.example.futuremind.data.remote.RemoteItemSource
import com.example.futuremind.data.remote.service.ApiService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import retrofit2.Retrofit
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
abstract class DataModule {

    companion object {

        @Provides
        fun providesStringFormatter() = StringFormatter()

        @Provides
        @Singleton
        fun providesRemoteItemSource(
            apiService: ApiService
        ) = RemoteItemSource(
            apiService
        )

        @Provides
        @Singleton
        fun providesLocalItemSource(
            database: ItemsDatabase
        ) = LocalItemSource(
            database.itemsDao()
        )

        @Provides
        @Singleton
        fun providesItemRepository(
            remoteItemSource: RemoteItemSource,
            localItemSource: LocalItemSource,
            dispatcher: CoroutineDispatcher,
            stringFormatter: StringFormatter
        ): ItemRepository = ItemRepositoryImpl(
            remoteItemSource,
            localItemSource,
            dispatcher,
            stringFormatter
        )

        @Singleton
        @Provides
        fun provideDataBase(@ApplicationContext context: Context): ItemsDatabase {
            return Room.databaseBuilder(
                context.applicationContext,
                ItemsDatabase::class.java,
                BuildConfig.DATABASE_NAME
            )
                .build()
        }

        @Singleton
        @Provides
        fun provideIoDispatcher() = Dispatchers.IO

        @Singleton
        @Provides
        fun provideApiService(retrofit: Retrofit): ApiService =
            retrofit.create(ApiService::class.java)

    }
}