package com.example.futuremind

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class FutureMindApplication : Application()