package com.example.futuremind.data

import android.util.Patterns

private const val DELIMITER = "\t"
private const val EMPTY_STRING = ""

class StringFormatter {

    fun splitDescription(description: String) =
        Pair(
            description.substringBeforeLast(DELIMITER),
            getUrl(description.substringAfterLast(DELIMITER))
        )

    private fun getUrl(string: String): String {
        return if (Patterns.WEB_URL.matcher(string).matches()) {
            string
        } else {
            EMPTY_STRING
        }
    }
}