package com.example.futuremind.data.local.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.futuremind.data.Item
import com.example.futuremind.data.local.db.converter.DateConverter

@Database(entities = [Item::class], version = 1, exportSchema = false)
@TypeConverters(DateConverter::class)
abstract class ItemsDatabase : RoomDatabase() {

    abstract fun itemsDao(): ItemsDao
}