package com.example.futuremind.data.local

import com.example.futuremind.data.Item
import com.example.futuremind.data.local.db.ItemsDao
import javax.inject.Inject

class LocalItemSource @Inject constructor(private val itemsDao: ItemsDao) {

    suspend fun getItems(): List<Item> = itemsDao.getAll()

    suspend fun setItems(list: List<Item>) = itemsDao.setItems(list)
}