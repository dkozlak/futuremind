package com.example.futuremind.data.remote.dto

import com.google.gson.annotations.SerializedName
import java.time.LocalDate

data class ItemDto(
    @SerializedName("description") val description: String,
    @SerializedName("image_url") val imageUrl: String,
    @SerializedName("modificationDate") val modificationDate: LocalDate,
    @SerializedName("orderId") val orderId: Int,
    @SerializedName("title") val title: String
)