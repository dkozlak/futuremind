package com.example.futuremind.data.remote

import com.example.futuremind.data.remote.dto.ItemDto
import com.example.futuremind.data.remote.service.ApiService
import javax.inject.Inject

class RemoteItemSource @Inject constructor(private val apiService: ApiService) {

    suspend fun getItems(): List<ItemDto> = apiService.getItems()
}