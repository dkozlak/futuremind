package com.example.futuremind.data

import com.example.futuremind.data.local.LocalItemSource
import com.example.futuremind.data.remote.RemoteItemSource
import com.example.futuremind.data.remote.Result
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import javax.inject.Inject

class ItemRepositoryImpl @Inject constructor(
    private val remoteItemSource: RemoteItemSource,
    private val localItemSource: LocalItemSource,
    private val dispatcher: CoroutineDispatcher,
    private val stringFormatter: StringFormatter
): ItemRepository {

    override suspend fun getItemsFromRemote() =
        withContext(dispatcher) {
            try {
                Result.Success(remoteItemSource.getItems().map {
                    val splitDescription = stringFormatter.splitDescription(it.description)
                    Item(
                        title = it.title,
                        description = splitDescription.first,
                        url = splitDescription.second,
                        imageUrl = it.imageUrl,
                        modificationDate = it.modificationDate,
                        orderId = it.orderId
                    )
                })
            } catch (exception: Exception) {
                Result.Error(exception)
            }
        }


    override suspend fun getItemsFromLocal() =
        withContext(dispatcher) {
            localItemSource.getItems()
        }


    override suspend fun setItemsInLocal(list: List<Item>) =
        withContext(dispatcher) {
            localItemSource.setItems(list)
        }

}

interface ItemRepository {

    suspend fun getItemsFromRemote(): Result<List<Item>>
    suspend fun getItemsFromLocal(): List<Item>
    suspend fun setItemsInLocal(list: List<Item>)
}