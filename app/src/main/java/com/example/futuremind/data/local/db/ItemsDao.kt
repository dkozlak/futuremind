package com.example.futuremind.data.local.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction
import com.example.futuremind.data.Item

@Dao
interface ItemsDao {

    @Query("SELECT * FROM item")
    suspend fun getAll(): List<Item>

    @Query("DELETE FROM item")
    suspend fun deleteAll()

    @Insert
    suspend fun insertAll(itemList: List<Item>)

    @Transaction
    suspend fun setItems(itemList: List<Item>) {
        deleteAll()
        insertAll(itemList)
    }

}