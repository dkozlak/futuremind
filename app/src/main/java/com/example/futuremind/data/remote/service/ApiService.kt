package com.example.futuremind.data.remote.service

import com.example.futuremind.data.remote.dto.ItemDto
import retrofit2.http.GET

interface ApiService {

    @GET("recruitment-task")
    suspend fun getItems(): List<ItemDto>
}