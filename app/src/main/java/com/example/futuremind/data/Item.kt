package com.example.futuremind.data

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.time.LocalDate

@Entity
data class Item(
    @NonNull @ColumnInfo(name = "title") val title: String,
    @NonNull @ColumnInfo(name = "description") val description: String,
    @NonNull @ColumnInfo(name = "image_url") val imageUrl: String,
    @NonNull @ColumnInfo(name = "url") val url: String,
    @NonNull @ColumnInfo(name = "modification_date") val modificationDate: LocalDate,
    @NonNull @ColumnInfo(name = "order_id") val orderId: Int
) {
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
}