package com.example.futuremind.domain

import com.example.futuremind.data.ItemRepository
import com.example.futuremind.view.list.viewmodel.ItemListUiState
import javax.inject.Inject

class GetItemsUseCase @Inject constructor(private val itemRepository: ItemRepository) {

    suspend operator fun invoke(): ItemListUiState {
        val result = itemRepository.getItemsFromLocal()
        return if (result.isEmpty()) {
            ItemListUiState.Empty
        } else {
            ItemListUiState.Success(result.sortedBy { it.orderId })
        }
    }
}