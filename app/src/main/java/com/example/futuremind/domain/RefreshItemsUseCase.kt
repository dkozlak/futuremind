package com.example.futuremind.domain

import com.example.futuremind.data.ItemRepository
import com.example.futuremind.data.remote.Result
import com.example.futuremind.view.list.viewmodel.ItemListUiState
import javax.inject.Inject

class RefreshItemsUseCase @Inject constructor(private val itemRepository: ItemRepository) {

    suspend operator fun invoke(): ItemListUiState {
        return when (val result = itemRepository.getItemsFromRemote()) {
            is Result.Success -> {
                itemRepository.setItemsInLocal(result.data)
                if (result.data.isNotEmpty())
                    ItemListUiState.Success(result.data)
                else
                    ItemListUiState.Loading
            }
            is Result.Error -> {
                ItemListUiState.Error(result.exception)
            }
        }
    }
}