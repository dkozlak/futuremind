package com.example.futuremind.view.list.adapter

import androidx.recyclerview.widget.DiffUtil
import com.example.futuremind.data.Item

class ItemDiffUtil(private val oldList: List<Item>, private val newList: List<Item>) :
    DiffUtil.Callback() {

    override fun getOldListSize(): Int = oldList.size

    override fun getNewListSize(): Int = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
        oldList[oldItemPosition].id == newList[newItemPosition].id


    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
        oldList[oldItemPosition] == newList[newItemPosition]
}