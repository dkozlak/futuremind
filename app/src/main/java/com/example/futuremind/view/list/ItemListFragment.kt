package com.example.futuremind.view.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.example.futuremind.R
import com.example.futuremind.databinding.FragmentItemListBinding
import com.example.futuremind.view.detail.ItemDetailFragment
import com.example.futuremind.view.list.adapter.ItemRecyclerViewAdapter
import com.example.futuremind.view.list.viewmodel.ItemListUiState
import com.example.futuremind.view.list.viewmodel.ListViewModel
import dagger.hilt.android.AndroidEntryPoint

private const val EMPTY_URL = ""

@AndroidEntryPoint
class ItemListFragment : Fragment() {

    private val viewModel: ListViewModel by viewModels()

    private var _binding: FragmentItemListBinding? = null
    private val binding get() = _binding!!

    private val isTablet by lazy { resources.getBoolean(R.bool.isTablet) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentItemListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
        setSwipeToRefreshListener()
        viewModel.uiState.observe(viewLifecycleOwner, ::handleUiState)
        viewModel.navigationEvent.observe(viewLifecycleOwner, ::handleNavigation)
        viewModel.init()
    }

    private fun handleUiState(state: ItemListUiState) {
        when (state) {
            is ItemListUiState.Success -> {
                hideLoading()
                showList()
                (binding.list.listLayoutRecyclerView.adapter as ItemRecyclerViewAdapter).setData(
                    state.itemsList
                )
            }
            is ItemListUiState.Error -> {
                hideLoading()
                Toast.makeText(
                    requireContext(),
                    state.exception.localizedMessage,
                    Toast.LENGTH_SHORT
                ).show()
            }
            ItemListUiState.Loading -> {
                showLoading()
            }
            ItemListUiState.Empty -> {
                hideLoading()
                showEmptyState()
            }
        }
    }

    private fun handleNavigation(url: String?) {
        if (url == null) {
            if (isTablet) {
                navigateToItemDetails(EMPTY_URL)
            }
        } else {
            navigateToItemDetails(url)
        }
    }

    private fun showLoading() {
        binding.list.listLayoutProgressBar.visibility = View.VISIBLE
    }

    private fun hideLoading() {
        binding.list.listLayoutProgressBar.visibility = View.GONE
    }

    private fun showList() {
        binding.list.listLayoutRecyclerView.visibility = View.VISIBLE
        binding.list.listLayoutEmptyStateText.visibility = View.GONE
    }

    private fun showEmptyState() {
        binding.list.listLayoutRecyclerView.visibility = View.GONE
        binding.list.listLayoutEmptyStateText.visibility = View.VISIBLE
    }

    private fun setupRecyclerView() {
        binding.list.listLayoutRecyclerView.adapter =
            ItemRecyclerViewAdapter { viewModel.itemClicked(it) }
    }

    private fun setSwipeToRefreshListener() {
        binding.itemListSwipeRefresh.setOnRefreshListener {
            binding.itemListSwipeRefresh.isRefreshing = false
            viewModel.pullToRefresh()
        }
    }

    private fun navigateToItemDetails(url: String) {
        val bundle = Bundle().apply { putString(ItemDetailFragment.ARG_ITEM_URL, url) }
        if (isTablet) {
            val itemDetailFragmentContainer: View? =
                view?.findViewById(R.id.item_detail_nav_container)
            itemDetailFragmentContainer?.findNavController()
                ?.navigate(R.id.fragment_item_detail, bundle)
        } else {
            findNavController().navigate(R.id.show_item_detail, bundle)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}