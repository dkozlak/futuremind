package com.example.futuremind.view.list.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.futuremind.data.Item
import com.example.futuremind.domain.GetItemsUseCase
import com.example.futuremind.domain.RefreshItemsUseCase
import com.example.futuremind.utils.SingleLiveEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ListViewModel @Inject constructor(
    private val getItemsUseCase: GetItemsUseCase,
    private val refreshItemsUseCase: RefreshItemsUseCase
) : ViewModel() {

    private val _navigationEvent: SingleLiveEvent<String?> = SingleLiveEvent()
    val navigationEvent: LiveData<String?> = _navigationEvent

    private val _uiState: MutableLiveData<ItemListUiState> =
        MutableLiveData(ItemListUiState.Loading)
    val uiState: LiveData<ItemListUiState> = _uiState

    fun init() {
        _uiState.value = ItemListUiState.Loading
        viewModelScope.launch {
            val result = getItemsUseCase()
            if (result is ItemListUiState.Empty) {
                val refreshResult = refreshItemsUseCase()
                _uiState.value = refreshResult
            } else {
                _uiState.value = result
            }
        }
    }

    fun pullToRefresh() {
        _uiState.value = ItemListUiState.Loading
        viewModelScope.launch {
            val refreshResult = refreshItemsUseCase()
            _uiState.value = refreshResult
            if (refreshResult is ItemListUiState.Success) _navigationEvent.value = null
        }
    }

    fun itemClicked(url: String) {
        _navigationEvent.value = url
    }

}

sealed class ItemListUiState {
    data class Success(val itemsList: List<Item>) : ItemListUiState()
    data class Error(val exception: Throwable) : ItemListUiState()
    object Loading : ItemListUiState()
    object Empty : ItemListUiState()
}
