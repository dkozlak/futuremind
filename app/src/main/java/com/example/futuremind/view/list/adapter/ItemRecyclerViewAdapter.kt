package com.example.futuremind.view.list.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.futuremind.data.Item
import com.example.futuremind.databinding.ListItemBinding

class ItemRecyclerViewAdapter(
    private val adapterOnClick: (String) -> Unit
) : RecyclerView.Adapter<ViewHolder>() {

    private val items: MutableList<Item> = mutableListOf()

    fun setData(list: List<Item>) {
        val diffCallback = ItemDiffUtil(items, list)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        items.clear()
        items.addAll(list)
        diffResult.dispatchUpdatesTo(this)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            ListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        holder.bind(item, adapterOnClick)
    }

    override fun getItemCount() = items.size

}