package com.example.futuremind.view.list.adapter

import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.futuremind.data.Item
import com.example.futuremind.databinding.ListItemBinding

class ViewHolder(private val binding: ListItemBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(item: Item, adapterOnClick: (String) -> Unit) {
        with(binding) {
            listItemDate.text = item.modificationDate.toString()
            listItemTitle.text = item.title
            listItemDescription.text = item.description
            listItemImage.load(item.imageUrl)
            binding.root.setOnClickListener { adapterOnClick(item.url) }
        }
    }
}