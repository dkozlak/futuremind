package com.example.futuremind

import com.example.futuremind.data.ItemRepository
import com.example.futuremind.data.ItemRepositoryImpl
import com.example.futuremind.data.StringFormatter
import com.example.futuremind.data.local.LocalItemSource
import com.example.futuremind.data.remote.RemoteItemSource
import com.example.futuremind.data.remote.Result
import com.example.futuremind.stub.StubItems
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class ItemRepositoryUnitTest {

    @ExperimentalCoroutinesApi
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    @MockK
    private val remoteItemSource: RemoteItemSource = mockk()

    @MockK
    private val localItemSource: LocalItemSource = mockk()

    @MockK
    private val stringFormatter: StringFormatter = mockk()

    private lateinit var itemRepository: ItemRepository

    @ExperimentalCoroutinesApi
    @Before
    fun setup() {
        MockKAnnotations.init(this)
        itemRepository = ItemRepositoryImpl(remoteItemSource, localItemSource, Dispatchers.Main, stringFormatter)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `should return empty list from remote`() = mainCoroutineRule.runBlockingTest {
        val remoteList = StubItems.emptyDtoList
        val mappedList = StubItems.emptyList
        coEvery { remoteItemSource.getItems() } returns remoteList

        val task = itemRepository.getItemsFromRemote()

        assertEquals((task as Result.Success).data, mappedList)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `should return filled list from remote`() = mainCoroutineRule.runBlockingTest {
        val remoteList = StubItems.filledDtoList
        val mappedList = StubItems.filledList
        coEvery { remoteItemSource.getItems() } returns remoteList
        every { stringFormatter.splitDescription(any()) } returns Pair(StubItems.item.description, StubItems.item.url)

        val task = itemRepository.getItemsFromRemote()

        assertEquals((task as Result.Success).data, mappedList)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `should return error list from remote`() = mainCoroutineRule.runBlockingTest {
        coEvery { remoteItemSource.getItems() } throws Exception()

        val task = itemRepository.getItemsFromRemote()

        assert(task is Result.Error)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `should return filled list from db`() = mainCoroutineRule.runBlockingTest {
        val list = StubItems.filledList
        coEvery { localItemSource.getItems() } returns list

        val task = itemRepository.getItemsFromLocal()

        assertEquals(task, list)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `should return empty list from db`() = mainCoroutineRule.runBlockingTest {
        val list = StubItems.emptyList
        coEvery { localItemSource.getItems() } returns list

        val task = itemRepository.getItemsFromLocal()

        assertEquals(task, list)
    }
}