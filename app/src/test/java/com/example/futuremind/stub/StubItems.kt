package com.example.futuremind.stub

import com.example.futuremind.data.Item
import com.example.futuremind.data.remote.dto.ItemDto
import java.time.LocalDate

object StubItems {

    val modificationDate: LocalDate = LocalDate.of(2021, 12, 31)

    val itemDto = ItemDto(
        "test-description\thttps://www.google.com",
        "test-image-url",
        modificationDate,
        0,
        "test-title"
    )

    val item = Item(
        "test-title",
        "test-description",
        "test-image-url",
        "https://www.google.com",
        modificationDate,
        0,
    )

    val emptyDtoList = emptyList<ItemDto>()

    val emptyList = emptyList<Item>()

    val filledDtoList = listOf(
        itemDto.copy(orderId = 1),
        itemDto.copy(orderId = 0)
    )
    val filledList = listOf(
        item.copy(orderId = 1),
        item.copy(orderId = 0)
    )
}